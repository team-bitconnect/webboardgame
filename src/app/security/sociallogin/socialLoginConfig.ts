import {environment} from '../../../environments/environment';
import {AuthServiceConfig, FacebookLoginProvider, GoogleLoginProvider} from 'ng4-social-login';

export function provideConfig() {
  return new AuthServiceConfig([
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider(environment.facebookClientId)
    },
    {
      id: GoogleLoginProvider.PROVIDER_ID,
      provider: new GoogleLoginProvider(environment.googleClientId)
    }
  ], false);
}

