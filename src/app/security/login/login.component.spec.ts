import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LoginComponent} from './login.component';
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {AuthenticationService} from '../_services/authentication.service';
import {ReactiveFormsModule} from '@angular/forms';
import {MatCardModule, MatFormFieldModule, MatInputModule} from '@angular/material';
import {RouterTestingModule} from '@angular/router/testing';
import {HomeComponent} from '../../main/home/home.component';
import {Router} from '@angular/router';
import {appRoutes} from '../../app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {GameComponent} from '../../main/game/game.component';
import {HttpClient, HttpHandler} from '@angular/common/http';
import {AccountComponent} from '../account/account.component';
import {RegisterComponent} from '../register/register.component';
import {OverviewComponent} from '../../main/overview/overview.component';


describe('LoginComponent', () => {
  let loginComponent: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let de: DebugElement;
  let router: Router;

  // given
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent,
        HomeComponent,
        GameComponent,
        AccountComponent,
        RegisterComponent,
        OverviewComponent],
      providers: [AuthenticationService, HttpClient, Router, HttpHandler],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatInputModule,
        MatFormFieldModule,
        MatCardModule,
        RouterTestingModule.withRoutes(appRoutes)]
    }).compileComponents();
  }));

  // when
  beforeEach(() => {
    // routing
    router = TestBed.get(Router);

    // loginComponent
    fixture = TestBed.createComponent(LoginComponent);
    loginComponent = fixture.componentInstance;
    de = fixture.debugElement;

    // onChange
    fixture.detectChanges();
  });

  // then
  // it('should create login loginComponent', () => {
  //   expect(loginComponent).toBeTruthy();
  // });

  // it('should have valid credentials', () => {
  //   const email = loginComponent.loginForm.controls['email'];
  //   const password = loginComponent.loginForm.controls['password'];
  //
  //   // testing valid email adress
  //   email.setValue('test@testosteron.com');
  //   password.setValue('123456');
  //   expect(email.valid).toBeTruthy();
  //   expect(password.valid).toBeTruthy();
  //
  //   // testing invalid email adress
  //   email.setValue('test.testosteron.com');
  //   password.setValue('1234');
  //   expect(email.valid).toBeFalsy();
  //   expect(password.valid).toBeFalsy();
  // });
  //
  // it('should have an email field and a password field', () => {
  //   const email = loginComponent.loginForm.controls['email'];
  //   const password = loginComponent.loginForm.controls['password'];
  //   // testing presence of credential fields
  //   expect(email).not.toBeNull();
  //   expect(password).not.toBeNull();
  // });
  //
  // it('when i click on the login button i should navigate to the home loginComponent', async(() => {
  //   // spies on submit method in logincomponent
  //   spyOn(loginComponent, 'login');
  //
  //   // click on login button
  //   const loginButton = fixture.debugElement.nativeElement.querySelector('button');
  //   expect(loginButton).toBeTruthy();
  //   loginButton.click();
  //
  //   // check if submit button has been called and if we've landed on the correct url
  //   fixture.whenStable().then(() => {
  //     expect(loginComponent.login).toHaveBeenCalled();
  //     expect(router.url).toBe('/');
  //   });
  // }));
});
