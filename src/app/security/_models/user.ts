import {Turn} from '../../main/replays/_models/turn';
import {Statistic} from '../../main/statistics/_models/statistic';
import {Game} from '../../main/game/_models/game';
import {Score} from '../../main/game/_models/score';
import {ChatMessage} from '../../main/chat/_models/chatmessage';
import {LetterRack} from '../../main/game/_models/letterrack';

export class User {
  email: string;
  password: string;
  username: string;
  avatar: string;
  accountIsActivated: boolean;
  friends: Set<User>;
  turns: Turn[];
  games: Game[];
  scores: Score[];
  chatMessages: ChatMessage[];
  statistic: Statistic;
  letterRacks: LetterRack[];
  id: string;

  constructor(username?: string, email?: string, avatar?: string) {
    this.username = username;
    this.email = email;
    this.avatar = avatar;
  }
}
