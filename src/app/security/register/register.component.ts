import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../_services/authentication.service';
import {RegisterDTO} from '../_dto/RegisterDTO';


/**
 * component used to register new users
 */
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  registeredUser: RegisterDTO;
  loading = false;
  returnUrl: string;
  error = '';

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private authService: AuthenticationService) {
  }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });

    this.registeredUser = new RegisterDTO();

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get form() {
    return this.registerForm.controls;
  }

  onRegister() {
    if (this.registerForm.valid) {
      this.registeredUser.username = this.registerForm.controls.username.value;
      this.registeredUser.email = this.registerForm.controls.email.value;
      this.registeredUser.password = this.registerForm.controls.password.value;
      console.log('user to be registered: ' + JSON.stringify(this.registeredUser));

      this.authService.register(this.registeredUser).subscribe((response) => {
          console.log('register response:' + response.toString());
          this.router.navigateByUrl('/login');
        }, (err) => console.error(err)
      );
    }
  }
}
