import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RegisterComponent} from './register.component';
import {Router} from '@angular/router';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('RegisterComponent', () => {
  let router: Router;
  let registerComponent: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [RegisterComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    router = TestBed.get(Router);
    fixture = TestBed.createComponent(RegisterComponent);
    registerComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(registerComponent).toBeTruthy();
  // });
  //
  // it('should have valid credentials', () => {
  //   const email = registerComponent.registerForm.controls['email'];
  //   const password = registerComponent.registerForm.controls['password'];
  //
  //   // testing valid email adress
  //   email.setValue('test@testosteron.com');
  //   password.setValue('123456');
  //   expect(email.valid).toBeTruthy();
  //   expect(password.valid).toBeTruthy();
  //
  //   // testing invalid email adress
  //   email.setValue('test.testosteron.com');
  //   password.setValue('1234');
  //   expect(email.valid).toBeFalsy();
  //   expect(password.valid).toBeFalsy();
  // });
  //
  // it('should have an email field and a password field', () => {
  //   const email = registerComponent.registerForm.controls['email'];
  //   const password = registerComponent.registerForm.controls['password'];
  //   // testing presence of credential fields
  //   expect(email).not.toBeNull();
  //   expect(password).not.toBeNull();
  // });
  //
  // it('when i click on the register button i should navigate to the home registerComponent', async(() => {
  //   // spies on submit method in logincomponent
  //   spyOn(registerComponent, 'onRegister');
  //
  //   // click on register button
  //   const registerButton = fixture.debugElement.nativeElement.querySelector('button');
  //   expect(registerButton).toBeTruthy();
  //   registerButton.click();
  //
  //   // check if submit button has been called and if we've landed on the correct url
  //   fixture.whenStable().then(() => {
  //     expect(registerButton.onRegister).toHaveBeenCalled();
  //     expect(router.url).toBe('/login');
  //   });
  // }));
});
