import {TestBed} from '@angular/core/testing';

import {AuthenticationService} from './authentication.service';
import {GameComponent} from '../../main/game/game.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {HttpClient, HttpHandler} from '@angular/common/http';
import {Router, RouterModule} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {appRoutes} from '../../app-routing.module';
import {AccountComponent} from '../account/account.component';
import {LoginComponent} from '../login/login.component';
import {HomeComponent} from '../../main/home/home.component';
import {RegisterComponent} from '../register/register.component';
import {OverviewComponent} from '../../main/overview/overview.component';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule, MatFormFieldModule, MatInputModule} from '@angular/material';

describe('AuthenticationService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: [LoginComponent,
      HomeComponent,
      GameComponent,
      AccountComponent,
      RegisterComponent,
      OverviewComponent], // actorviews in this test    schemas: [],
    providers: [HttpHandler, HttpClient],
    schemas: [CUSTOM_ELEMENTS_SCHEMA], // needed for board and game
    imports: [ReactiveFormsModule, BrowserAnimationsModule, MatInputModule, RouterModule,
      MatFormFieldModule, MatCardModule, RouterTestingModule.withRoutes(appRoutes)] // dependencies

  }));

  // it('should be created', () => {
  //   const service: AuthenticationService = TestBed.get(AuthenticationService);
  //   // expect(service).toBeTruthy();
  // });
});
