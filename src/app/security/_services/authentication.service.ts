import {Injectable, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {RegisterDTO} from '../_dto/RegisterDTO';
import {User} from '../_models/user';
import {async} from 'rxjs/internal/scheduler/async';


/**
 * service to authenticate users and to use data from the logged in user
 */
@Injectable({providedIn: 'root'})
export class AuthenticationService {
  currentUserSubject = new BehaviorSubject<any>('');
  usernameSubject = new BehaviorSubject<any>('');
  currentUser = this.currentUserSubject.asObservable();


  constructor(private http: HttpClient, private router: Router) {
    this.currentUserSubject.next(localStorage.getItem('user_data'));
    if (this.currentUserSubject.value != null) {
      this.usernameSubject.next(JSON.parse(localStorage.getItem('user_data')).username);
    }
  }

  register(registeredUser: RegisterDTO): Observable<any> {
    return this.http.post(`${environment.baseUrl}/api/register`, registeredUser, {responseType: 'text'}).pipe(
      map((data => {
          console.log(data.toString());
          return data;
        }
      )));
  }

  getUserName() {
    return this.usernameSubject.getValue();
  }

  getUserByEmail(email: string): Observable<any> {
    return this.http.get<any>(`${environment.baseUrl}/api/user/${email}`)
      .pipe(map(user => {
        return user;
      }));
  }

  // when logging in this will be called
  tokenRequest(username: string, password: string): Observable<any> {
    const formData = new FormData();

    // append your data
    formData.append('username', username);
    formData.append('password', password);
    formData.append('grant_type', 'password');

    localStorage.setItem('email', username);

    // pass credentials to oauth server trough headers
    return this.http.post(
      environment.baseUrl + '/oauth/token',
      formData,
      {headers: this.getAuthHeaders()});
  }

  decodeJWT(token: string) {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace('-', '+').replace('_', '/');
    const parsedObj = JSON.parse(window.atob(base64));
    console.log('parsedObj: ' + JSON.stringify(parsedObj));
    return parsedObj;
  }

  // remove user from storage and from subject
  removeUserData() {
    localStorage.removeItem(environment.localStorageToken);
    localStorage.removeItem(environment.currentUser);
    localStorage.setItem('logged_in', 'false');

    this.currentUser = null;
    this.usernameSubject.next(null);
    this.currentUserSubject.next(null);
  }

  getAuthHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Authorization': 'Basic ' + btoa(environment.clientid + ':' + environment.clientsecret),
    });
  }
}
