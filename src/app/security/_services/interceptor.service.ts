import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {AuthenticationService} from './authentication.service';


/**
 * HttpInterceptor will attach a jwt to the request header before it's sent to the server
 * if the token is not present we'll pass the authorization header with our credentials for the oauth2 server
 * if the token is present we'll send the authorization header with our jwt token attached
 */

@Injectable({
  providedIn: 'root'
})
export class HttpConfigInterceptor implements HttpInterceptor {

  TOKEN_HEADER_KEY = 'Authorization';
  constructor(private router: Router, private  authService: AuthenticationService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authReq = req;
    const access_token = localStorage.getItem(environment.localStorageToken);

    if (access_token !== '' && access_token != null) {
      console.log(`access_token present: ${access_token}`);
      authReq = req.clone({headers: req.headers.set(this.TOKEN_HEADER_KEY, `Bearer ${access_token}`)});
    } else {
      authReq = req.clone({headers: this.authService.getAuthHeaders()});
    }

    return next.handle(authReq).pipe(map((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        console.log('httprequest ---->>>', event);
      }
      return event;
    }));
  }
}
