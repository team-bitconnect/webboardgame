import {Game} from '../../main/game/_models/game';
import {Score} from '../../main/game/_models/score';

export class UserDTO {
  username: string;
  email: string;
  avatar: string;
  activated: boolean;
  games: Game[];
  scores: Score[];
}
