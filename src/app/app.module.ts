import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import {appRoutes, AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {RegisterComponent} from './security/register/register.component';
import {HomeComponent} from './main/home/home.component';
import {AccountComponent} from './security/account/account.component';
import {LoginComponent} from './security/login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  MatBadgeModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatOptionModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatTabsModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatExpansionModule, MatProgressSpinnerModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule, HttpInterceptor} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import {GameComponent} from './main/game/game.component';
import {BoardComponent} from './main/board/board.component';
import {AuthenticationService} from './security/_services/authentication.service';
import {AuthGuard} from './security/_guards/auth.guard';
import {AuthServiceConfig, SocialLoginModule} from 'ng4-social-login';
import {provideConfig} from './security/sociallogin/socialLoginConfig';
import {HttpConfigInterceptor} from './security/_services/interceptor.service';
import { OverviewComponent } from './main/overview/overview.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { NewgameComponent } from './main/newgame/newgame.component';
import { StatisticsComponent } from './main/statistics/statistics.component';
import { FriendsComponent } from './main/friends/friends.component';
import { ReplaysComponent } from './main/replays/replays.component';
import { ChatComponent } from './main/chat/chat.component';
import {InjectableRxStompConfig, RxStompService, rxStompServiceFactory, StompConfig, StompService} from '@stomp/ng2-stompjs';
import {myRxStompConfig} from './my-rx-stomp.config';
import { ReplayDetailComponent } from './main/replay-detail/replay-detail.component';
import { NotificationsComponent } from './main/notifications/notifications.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    AccountComponent,
    BoardComponent,
    GameComponent,
    OverviewComponent,
    NewgameComponent,
    StatisticsComponent,
    FriendsComponent,
    ReplaysComponent,
    ChatComponent,
    ReplayDetailComponent,
    NotificationsComponent

  ],
  imports: [
    MatProgressSpinnerModule,
    MatExpansionModule,
    FormsModule,
    BrowserModule,
    MatInputModule,
    MatSliderModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    MatCardModule,
    MatBadgeModule,
    MatButtonToggleModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatTabsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    MatTooltipModule,
    RouterModule.forRoot(appRoutes, {enableTracing: false}),
    SocialLoginModule,
    DragDropModule
  ],
  providers: [
    StompService,
    StompConfig,
    AuthenticationService,
    AuthGuard,
    {
      provide: InjectableRxStompConfig,
      useValue: myRxStompConfig
    },
    {
      provide: RxStompService,
      useFactory: rxStompServiceFactory,
      deps: [InjectableRxStompConfig]
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpConfigInterceptor,
      multi: true
    },
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
