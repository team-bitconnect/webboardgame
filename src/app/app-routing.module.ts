import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './security/login/login.component';
import {HomeComponent} from './main/home/home.component';
import {AuthGuard} from './security/_guards/auth.guard';
import {GameComponent} from './main/game/game.component';
import {RegisterComponent} from './security/register/register.component';
import {AccountComponent} from './security/account/account.component';
import {OverviewComponent} from './main/overview/overview.component';
import {NewgameComponent} from './main/newgame/newgame.component';
import {StatisticsComponent} from './main/statistics/statistics.component';
import {FriendsComponent} from './main/friends/friends.component';
import {ReplaysComponent} from './main/replays/replays.component';
import {ReplayDetailComponent} from './main/replay-detail/replay-detail.component';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}


/**
 * routes where we can navigate to
 */
export const appRoutes: Routes = [
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'game/:id', component: GameComponent, canActivate: [AuthGuard]}, // route to a game with a certain id
  {path: 'account', component: AccountComponent, canActivate: [AuthGuard]},
  {path: 'newgame', component: NewgameComponent, canActivate: [AuthGuard]},
  {path: 'statistics', component: StatisticsComponent, canActivate: [AuthGuard]},
  {path: 'replays', component: ReplaysComponent, canActivate: [AuthGuard]},
  {path: 'replays/:id', component: ReplayDetailComponent, canActivate: [AuthGuard]},
  {path: 'friends', component: FriendsComponent, canActivate: [AuthGuard]},
  {path: 'overview', component: OverviewComponent, canActivate: [AuthGuard]},
  {path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
  {path: '  ', component: HomeComponent, canActivate: [AuthGuard]},
  {path: '**', component: HomeComponent, canActivate: [AuthGuard]}
];
