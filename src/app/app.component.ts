import {Component, HostListener, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {AuthenticationService} from './security/_services/authentication.service';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {User} from './security/_models/user';
import {NotificationService} from './main/_services/notification.service';
import {GameService} from './main/_services/game.service';


@Component({
  selector: 'app-root', templateUrl: './app.component.html', styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnChanges {
  title = 'Scrabble';
  user: Observable<User>;
  desktop: boolean;

  constructor(private authService: AuthenticationService,
              private notifService: NotificationService,
              private gameService: GameService,
              private router: Router) {
  }

  logout() {
    this.authService.removeUserData();
    this.router.navigate(['/login']);
    console.log('logged out');
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.desktop = event.target.innerWidth >= 1000;
  }

  ngOnInit(): any {
    this.user = this.authService.currentUser;
    this.desktop = window.innerWidth >= 1000;
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes.toString());
  }
}
