import {User} from '../../../security/_models/user';

export class Statistic {
  player: User;
  gamesPlayed: number;
  gamesWon: number;
  avgScore: number;
  maxScore: number;
}
