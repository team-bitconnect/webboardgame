import {Component, OnInit} from '@angular/core';
import {Statistic} from './_models/statistic';
import {StatisticService} from '../_services/statistic.service';



/**
 * component to handle everything that has to do with statistics
 */
@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {

  statistic: Statistic = {
    player: null,
    gamesPlayed: 0,
    gamesWon: 0,
    avgScore: 0,
    maxScore: 0
  };

  constructor(private statisticService: StatisticService) {
  }

  ngOnInit() {
    this.statisticService.getStatistics().subscribe(data => {
      console.log(data);
      this.statistic.player = data.player;
      this.statistic.gamesPlayed = data.gamesPlayed;
      this.statistic.gamesWon = data.gamesWon;
      this.statistic.avgScore = Number(Math.round(Number(data.avgScore + 'e2')) + 'e-2');
      this.statistic.maxScore = data.maxScore;
    });
  }
}
