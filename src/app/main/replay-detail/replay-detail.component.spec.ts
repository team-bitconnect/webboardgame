import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReplayDetailComponent } from './replay-detail.component';

describe('ReplayDetailComponent', () => {
  let component: ReplayDetailComponent;
  let fixture: ComponentFixture<ReplayDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReplayDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplayDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
