import {Component, OnInit} from '@angular/core';
import {TurnDto} from '../replays/_dto/turn-dto';
import {GameService} from '../_services/game.service';
import {ActivatedRoute} from '@angular/router';
import {Turn} from '../replays/_models/turn';
import {Cell} from '../board/_models/cell';
import {LetterRack} from '../game/_models/letterrack';
import {Letter} from '../board/_models/letter';
import {environment} from '../../../environments/environment';
import {tileUrls} from '../../../environments/tiles';
import {BoardService} from '../_services/board.service';
import {Game} from '../game/_models/game';
import {Player} from '../game/_models/player';
import {GameDto} from '../game/_dto/game-dto';
import {Tile} from '../board/_models/tile';


/**
 * component used to display the actual replays
 */

@Component({
  selector: 'app-replay-detail',
  templateUrl: './replay-detail.component.html',
  styleUrls: ['./replay-detail.component.scss']
})
export class ReplayDetailComponent implements OnInit {
  // turnstate
  cells: Cell[][];
  racks: LetterRack[] = [];
  currentTurn: Turn = new Turn();
  tileUrls = tileUrls;
  turnCounter = -1;
  turnDtos: TurnDto[] = [];

  // turn interval
  intervalId;
  started = false;
  speed = 2;
  game: Game = new Game();

  constructor(private boardService: BoardService,
              private gameService: GameService,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    // route to this replay
    const id = this.activatedRoute.snapshot.paramMap.get('id');

    // some data needed from game
    this.gameService.getGameyById(id).subscribe((g: GameDto) => {
      console.log('is over:' + g.over);
      this.game = new Game(
        g.id, null,
        g.players.map(p => new Player(p.username, p.email, this.gameService.dataURItoBlob(p.avatar))),
        new Player(g.creator.username, g.creator.email, this.gameService.dataURItoBlob(g.creator.avatar)),
        g.currentlyPlaying, g.over, g.hasStarted, g.title);
    });

    // transform fetched turns by gameid from dto to turnobjects
    this.gameService.getAllTurnsByGameId(id).subscribe((turns: TurnDto[]) => {
      turns.forEach(t => {
        // t.users.map( p => p.color = )
        this.turnDtos.push(t);
      });
    });

    // first currentTurn is empty
    this.currentTurn = this.emptyTurn();
  }

  /**  initial turn state */
  emptyTurn(): Turn {
    this.cells = [];
    for (let x = 0; x < 15; x++) {
      this.cells[x] = [];
      for (let y = 0; y < 15; y++) {
        this.cells[x][y] = new Cell(x, y, '_', 0, environment.multipliers[x][y]);
      }
    }

    const users: Player[] = [];
    users.push(new Player('', '', ''));
    return new Turn(0, this.cells, 'n/a', 0, this.racks, users);
  }

  /** extract a turn object from a dto */
  createTurn(turnCounter: number) {
    const t = this.turnDtos[turnCounter];
    // load the boardstate of each turn
    const cells = [];
    for (let x = 0; x < 15; x++) {
      cells[x] = [];
      for (let y = 0; y < 15; y++) {
        cells[x][y] = new Cell(x, y, t.board.charAt(x * 15 + y), 0, environment.multipliers[x][y]);
      }
    }

    /** letterracks */
    const racks: LetterRack[] = [];
    const users: Player[] = [];
    t.users.map(tu => users.push(new Player(tu.username, tu.email, this.gameService.dataURItoBlob(tu.avatar))));
    t.racks.substr(0, t.racks.length - 1).split('#')
      .map((r, i) => {
          const u = t.users[i];
          const letters = r.split('').map(c => new Letter(c, 'en', environment.tileUrls[c.toUpperCase()]));
          const tiles = letters.map(l => new Tile(l, null));
          const player = new Player(u.username, u.email, this.gameService.dataURItoBlob(u.avatar));
          racks.push(new LetterRack(player, tiles));
        }
      );

    return new Turn(t.turnCounter, cells, t.player, t.score, racks, users);
  }

  /** go forward */
  onNextTurn(): boolean {
    if (this.turnCounter < this.turnDtos.length - 1) {

      this.turnCounter++;
      console.log(`switching to turn ${this.turnCounter}`);
      this.currentTurn = this.createTurn(this.turnCounter);

      // raise score
      this.currentTurn.racks
        .filter(r => r.player.username === this.currentTurn.player)
        .map(p => p.player.score += this.currentTurn.score);

      // debugging
      console.log(`turn ${this.turnCounter}`);
      this.currentTurn.board.map(c => c.map(c1 => {
        if (c1.char !== '_') {
          console.log(c1.char);
        }
      }));

      return true;
    }
    return false;
  }

  /** go back */
  onPreviousTurn() {
    this.pause();
    if (this.turnCounter >= 0) {
      this.turnCounter--;
      console.log(`switching to turn ${this.turnCounter}`);

      if (this.turnCounter === -1) {
        this.currentTurn = this.emptyTurn();
        return;
      }

      // update current turn
      this.currentTurn = this.createTurn(this.turnCounter);

      // lower score
      this.currentTurn.racks
        .filter(r => r.player.username === this.currentTurn.player)
        .map(r => r.player.score = this.currentTurn.score);

      // debugging
      console.log(`turn ${this.turnCounter}`);
      this.currentTurn.board.map(c => c.map(c1 => {
        if (c1.char !== '_') {
          console.log(c1.char);
        }
      }));
    }
  }

  /** stop interval and start a new interval with the updated speed */
  playAll() {
    this.started = true;
    this.intervalId = setInterval(() => {
      console.log(`switching to turn ${this.turnCounter}`);

      // stop interval
      if (this.turnCounter >= this.turnDtos.length) {
        // stop interval subscription
        clearInterval(this.intervalId);
        return;
      }

      this.onNextTurn();
    }, this.speed * 1000);
  }

  /** stop interval and start a new interval with the updated speed */
  onSliderChange() {
    if (this.started && this.turnCounter < this.turnDtos.length - 1) {
      clearInterval(this.intervalId);
      this.started = false;
      this.playAll();
    }
  }

  pause() {
    clearInterval(this.intervalId);
    this.started = false;
  }
}
