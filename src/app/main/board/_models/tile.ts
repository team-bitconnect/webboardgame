import {Letter} from './letter';
import {LetterRack} from '../../game/_models/letterrack';

export class Tile {
  x: number;
  y: number;
  letter: Letter;
  letterRack: LetterRack;
  id: string;

  constructor(letter?: Letter, letterRack?: LetterRack, x?: number, y?: number, id?: string) {
    this.id = id;
    this.letter = letter;
    this.letterRack = letterRack;
    this.x = x;
    this.y = y;
  }
}
