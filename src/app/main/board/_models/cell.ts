export class Cell {
  x: number;
  y: number;
  multiplier: number;
  char: string;
  score: number;


  constructor(x: number, y: number, char: string, score?: number, multiplier?: number) {
    this.x = x;
    this.y = y;
    this.multiplier = multiplier;
    this.char = char;
    this.score = score;
  }
}
