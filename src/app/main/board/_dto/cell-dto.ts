export class CellDto {
  x: number;
  y: number;
  multiplier: number;
  char: string;
  lang: string;
  score: number;
}
