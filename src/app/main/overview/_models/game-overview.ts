import {Player} from '../../game/_models/player';

export class GameOverview {
  id: String;
  title: string;
  currentlyPlaying: number;
  players: Array<Player>;
  creator: Player;
  isOver: boolean;
  hasStarted: boolean;
  winner: string;
  turns: number;

  constructor(id: string, title: string, currentlyPlaying: number, players: Array<Player>, creator: Player, isOver: boolean, hasStarted: boolean, turns: number, winner: string) {
    this.id = id;
    this.title = title;
    this.currentlyPlaying = currentlyPlaying;
    this.players = players;
    this.creator = creator;
    this.isOver = isOver;
    this.hasStarted = hasStarted;
    this.turns = turns;
    this.winner = winner;
  }
}
