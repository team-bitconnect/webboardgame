import {PlayerDTO} from '../../game/_dto/player-dto';

export class GameOverviewDTO {
  id: string;
  title: string;
  currentlyPlaying: number;
  players: Array<PlayerDTO>;
  creator: PlayerDTO;
  over: boolean;
  hasStarted: boolean;
  turns: number;
  winner: string;
}
