import {Component, OnInit} from '@angular/core';
import {GameService} from '../_services/game.service';
import {Player} from '../game/_models/player';
import {GameOverview} from './_models/game-overview';
import {GameOverviewDTO} from './_dto/game-overview-dto';
import {Router} from '@angular/router';
import {SubOverview} from './_models/sub-overview';
import {AuthenticationService} from '../../security/_services/authentication.service';
import {RxStompService} from '@stomp/ng2-stompjs';
import {BehaviorSubject, interval} from 'rxjs';


/**
 * component used to display all non-started, started and finished games
 */
@Component({selector: 'app-overview', templateUrl: './overview.component.html', styleUrls: ['./overview.component.scss']})
export class OverviewComponent implements OnInit {
  notStartedGames: Array<GameOverview> = [];
  startedGames: Array<GameOverview> = [];
  finishedGames: Array<GameOverview> = [];
  allGames: Array<SubOverview> = [];
  allGamesSubject = new BehaviorSubject<SubOverview[]>([]);

  constructor(private gameService: GameService,
              private rxStompService: RxStompService,
              private authService: AuthenticationService,
              private router: Router) {
  }

  ngOnInit() {
    this.gameService.getAllAcceptedGames().subscribe((data: Array<GameOverviewDTO>) => {
      data.map(g => {
        const creator = new Player(
          g.creator.username,
          g.creator.email,
          this.gameService.dataURItoBlob(g.creator.avatar));
        const players = g.players.map(p =>
          new Player(p.username, p.email, this.gameService.dataURItoBlob(p.avatar)));

        // filter to allocated array
        if (g.hasStarted && !g.over) {
          console.log(`adding ${g.title} to started games`);
          this.startedGames.push(
            new GameOverview(g.id, g.title, g.currentlyPlaying, players, creator, g.over, g.hasStarted, g.turns, g.winner));
        }

        if (!g.hasStarted && !g.over) {
          console.log(`adding ${g.title} to not started games`);
          this.notStartedGames.push(
            new GameOverview(g.id, g.title, g.currentlyPlaying, players, creator, g.over, g.hasStarted, g.turns, g.winner));
        }

        if (g.over) {
          console.log(`adding ${g.title} to finished games`);
          this.finishedGames.push(
            new GameOverview(g.id, g.title, g.currentlyPlaying, players, creator, g.over, g.hasStarted, g.turns, g.winner));
        }

        // initialize connection when loading in games,
        // every game will listen if it needs to be placed in the started category
      });

      this.finishedGames.forEach((g, i) =>
        this.timeOutDelete(g, 10000)
      );

      // push all games to gamecollection
      this.allGames.push(
        new SubOverview(this.notStartedGames, 'Not started'),
        new SubOverview(this.startedGames, 'Started'),
        new SubOverview(this.finishedGames, 'Finished')
      );


      this.allGamesSubject.next(this.allGames);
    });

    this.initializeOverviewConnection();
  }

  private initializeOverviewConnection() {
    // watch new games, on acceptance of a new game
    // new game added to overview of a certain player when he accepted the invite
    this.rxStompService.watch(`/games/new/receive/${this.authService.getUserName()}`)
      .subscribe((message) => {
        // parse message to object
        const {goDto, creator, players} = this.mapToGameOverview(message);
        // map to gameoverview
        const go = new GameOverview(goDto.id, goDto.title, goDto.currentlyPlaying, players, creator, goDto.over, goDto.hasStarted, goDto.turns, goDto.winner);

        console.log(`You've accepted to participate in this game: ${go}`);
        this.updateOverview(go, 'new');
      });

    // watch existing games, on start a started game will be sent to this topic
    this.rxStompService.watch(`/games/start/receive/${this.authService.getUserName()}`)
      .subscribe((message) => {

        // parse message to object
        const {goDto, creator, players} = this.mapToGameOverview(message);
        const go = new GameOverview(goDto.id, goDto.title, goDto.currentlyPlaying, players, creator, goDto.over, goDto.hasStarted, goDto.turns, goDto.winner);

        this.updateOverview(go, 'start');
      }, error => console.log(error));

    // watch finished games, on game over a finished game will be sent to this topic
    this.rxStompService.watch(`/games/end/receive/${this.authService.getUserName()}`)
      .subscribe((message) => {
        // parse message to object
        const {goDto, creator, players} = this.mapToGameOverview(message);
        const go = new GameOverview(goDto.id, goDto.title, goDto.currentlyPlaying, players, creator, goDto.over, goDto.hasStarted, goDto.turns, goDto.winner);
        console.log('FINISHED GAME: ');
        console.log(go);

        // will be deleted from overview and database after a set amount of time
        this.updateOverview(go, 'end');
      }, error => console.log(error));
  }

  private mapToGameOverview(message) {
    const goDto: GameOverviewDTO = JSON.parse(message.body);
    console.log(`Game has started: ${JSON.stringify(goDto)} `);
    const creator = new Player(
      goDto.creator.username,
      goDto.creator.email,
      this.gameService.dataURItoBlob(goDto.creator.avatar));
    const players = goDto.players.map(p =>
      new Player(p.username, p.email, this.gameService.dataURItoBlob(p.avatar)));
    return {goDto, creator, players};
  }

  private updateOverview(overviewGame: GameOverview, updateType: string) {
    let index = 0;
    this.allGames = [];

    switch (updateType) {
      case 'new':
        // push to non-started games
        this.notStartedGames.push(overviewGame);
        break;
      case 'start':
        // push to started games
        index = this.notStartedGames.indexOf(overviewGame);
        this.notStartedGames.splice(index, 1);
        this.startedGames.push(overviewGame);
        break;
      case 'end':
        // push to finished games
        index = this.notStartedGames.indexOf(overviewGame);
        this.startedGames.splice(index, 1);
        this.finishedGames.push(overviewGame);

        this.timeOutDelete(overviewGame, 10000);
        break;
    }

    this.allGames.push(
      new SubOverview(this.notStartedGames, 'Not started'),
      new SubOverview(this.startedGames, 'Started'),
      new SubOverview(this.finishedGames, 'Finished')
    );

    // update overview dynamically
    this.allGamesSubject.next(null);
    this.allGamesSubject.next(this.allGames);
  }

  timeOutDelete(overviewGame: GameOverview, timeOut: number) {
    const index = this.finishedGames.indexOf(overviewGame);
    setTimeout(() => {
      console.log('DELETING GAME: ' + overviewGame.id);
      this.finishedGames.splice(index, 1);
      this.gameService.deleteGameById(overviewGame.id).subscribe(value => {
        console.log('GAME DELETED: ' + JSON.stringify(value));
      });
    }, timeOut);
  }

  onSelectOverviewGame(id: String) {
    this.router.navigate([`/game/${id}`]);
  }
}
