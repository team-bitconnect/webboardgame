import {LaidDownTileDTO} from './laidDownTileDTO';

export class PlaceTilesRequestDto {
  tiles: Array<LaidDownTileDTO>;


  constructor(tiles: Array<LaidDownTileDTO>) {
    this.tiles = tiles;
  }
}
