export class ScoreDto {
  gameId: string;
  username: string;
  score = 0;
}
