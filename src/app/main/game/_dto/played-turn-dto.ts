import {WordScoreDto} from './word-score-dto';

export class PlayedTurnDto {
  words: WordScoreDto[];
  representation: String;
  board: String;
  over: boolean;
}
