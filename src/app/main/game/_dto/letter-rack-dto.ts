class LetterRackTileDto {
  id: string;
  char: string;
}

export class LetterRackDto {
  player: string;
  game: string;
  tiles: LetterRackTileDto[];
}
