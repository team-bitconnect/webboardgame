import {Player} from './player';
import {Cell} from '../../board/_models/cell';
import {Score} from './score';

export class Game {
  id: string;
  cells: Cell[][];
  players: Array<Player> = new Array<Player>();
  creator: Player = new Player('', '', '');
  currentlyPlaying: number;
  isOver = false;
  title = 'Scrabble Game';
  hasStarted: boolean;
  scores: Score[];
  winner: string;
  
  constructor(id?: string,
              cells?: Cell[][],
              players?: Array<Player>,
              creator?: Player,
              currentlyPlaying?: number,
              isOver?: boolean,
              hasStarted?: boolean,
              title?: string,
              scores?: Score[],
              winner?: string) {
    this.id = id;
    this.cells = cells;
    this.players = players;
    this.scores = scores;
    this.creator = creator;
    this.currentlyPlaying = currentlyPlaying;
    this.isOver = isOver;
    this.hasStarted = hasStarted;
    this.title = title;
    this.winner = winner;
  }
}
