import {Player} from './player';
import {Tile} from '../../board/_models/tile';

export class LetterRack {
  player: Player;
  tiles: Tile[];

  constructor(player: Player, letters: Tile[]) {
    this.player = player;
    this.tiles = letters;
  }
}
