export class NewGameDto {
  title: String = 'Scrabble Game';
  language: String = 'en';
  players: Array<string>;

  constructor(title: String, language: String, players: Array<string>) {
    this.title = title;
    this.language = language;
    this.players = players;
  }
}
