import {Component, OnInit} from '@angular/core';
import {GameService} from '../_services/game.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {FriendsService} from '../_services/friends.service';
import {GameDto} from '../game/_dto/game-dto';
import {Observable, of} from 'rxjs';
import {PlayerSelection} from './_models/player-selection';
import {Game} from '../game/_models/game';
import {AuthenticationService} from '../../security/_services/authentication.service';
import {PlayerDTO} from '../game/_dto/player-dto';
import {RxStompService} from '@stomp/ng2-stompjs';


/**
 * component to handle everything that has to do with a newgame
 */
@Component({selector: 'app-newgame', templateUrl: './newgame.component.html', styleUrls: ['./newgame.component.scss']})
export class NewgameComponent implements OnInit {

  // dynamically loaded friends or previous
  friendsObservable: Observable<boolean>;
  previousObservable: Observable<boolean>;

  // dynamic form
  createGameForm: FormGroup;

  // data for dropdowns
  playerTypes = ['None', 'Random', 'Friend', 'Previous'];
  previous = new Set([]);
  friends: Array<string> = [];
  players: Array<PlayerSelection> = [
    new PlayerSelection(),
    new PlayerSelection(),
    new PlayerSelection()];

  constructor(private gameService: GameService,
              private friendsService: FriendsService,
              private rxStompService: RxStompService,
              private authService: AuthenticationService,
              private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    // new game form
    this.buildForm();

    // friends
    this.friendsService.getAllFriends().subscribe((data: Array<any>) => {
      data.map((f: PlayerDTO) => {
        this.friends.push(f.username);
        console.log(`ive got ${this.friends.length} friends`);
      });

      // no friends == no dropdown selection of friends in ui
      this.friendsObservable = of(data.length > 0);
    });

    // all previous players
    this.gameService.getAllGames().subscribe((value: Array<Game>) => {
        value.map(g =>
          g.players.map(
            p => {
              if (p.username !== this.authService.getUserName()) {
                this.previous.add(p.username);
              }
            })
        );

        // no previous == no dropdown selection of previous in ui
        this.previousObservable = of(this.previous.size > 0);
      }
    );
  }

  buildForm() {
    const group = {title: ['', [Validators.required]]};
    for (const i of [0, 1, 2]) {
      group[`playertype${i}`] = new FormControl('', [Validators.required]);
      group[`chosenOpponent${i}`] = new FormControl('', [Validators.required]);
    }
    this.createGameForm = this.formBuilder.group(group);

    // listeners that track changes in the reactive form for a new game
    for (const i of [0, 1, 2]) {
      this.createGameForm.get(`playertype${i}`).valueChanges.subscribe(val => {
          const chosenOpponent = this.createGameForm.get(`chosenOpponent${i}`);

          if ('Friend'.match(val) || 'Previous'.match(val)) {
            console.log(`you chose ${val}: choose an opponent`);
            chosenOpponent.setValidators([Validators.required]);
            chosenOpponent.updateValueAndValidity();
          } else {
            console.log(`you chose ${val}: removing validation for friend/previous`);
            chosenOpponent.clearValidators();
            chosenOpponent.updateValueAndValidity();
          }
        }
      );
    }
  }

  game() {
    return this.createGameForm.controls;
  }

  onCreateGame() {
    const players: Array<string> = [];

    // filter data from form when submitted
    if (this.createGameForm.valid) {
      for (const i of [0, 1, 2]) {
        const opponentType = this.game()[`playertype${i}`].value;
        if (opponentType === 'Previous' || opponentType === 'Friend') {
          const opponentName = this.game()[`chosenOpponent${i}`].value;
          players.push(opponentName);
        } else {
          players.push(opponentType);
        }
      }

      this.gameService.createNewGame(this.game().title.value, players).subscribe((game: GameDto) => {

        // send new game to websocket topic
        this.rxStompService.publish(
          {
            destination: `/games/new/send`,
            body: JSON.stringify({game: game})
          });

        this.router.navigate([`/overview`]);
      });
    }
  }

  onOpponentSelect(val) {
    console.log(val);
  }

  isOpponentTypeSlotEmpty(i: number) {
    return this.game()[`playertype${i}`].invalid;
  }

  isOpponentSlotEmpty(i: number) {
    return this.game()[`chosenOpponent${i}`].invalid;
  }
}

