import {Component, Input, OnInit} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {RxStompService} from '@stomp/ng2-stompjs';
import {ChatMessage} from './_models/chatmessage';
import {Game} from '../game/_models/game';
import {AuthenticationService} from '../../security/_services/authentication.service';
import {ChatService} from '../_services/chat.service';
import {MessageDto} from './_dto/message-dto';
import {map} from 'rxjs/operators';
import {Score} from '../game/_models/score';



/**
 * component to handle everything that has to do with a chat
 */
@Component({selector: 'app-chat', templateUrl: './chat.component.html', styleUrls: ['./chat.component.scss']})
export class ChatComponent implements OnInit {
  @Input() game: Game;
  @Input() currentlyPlaying: number;
  @Input() scores: Score[] = [];
  chatSubject = new BehaviorSubject<ChatMessage[]>([]);
  messageContentToSend = '';
  error: string;
  username: string;

  constructor(private rxStompService: RxStompService,
              private authService: AuthenticationService,
              private chatService: ChatService) {
  }

  /**
   * on page load the history is fetched and the chatconnection is made
   */
  ngOnInit() {
    this.username = this.authService.getUserName();

    // listen to new messages
    this.initializeChatConnection();

    // fetch history and fill observable
    this.chatService.getMessagesForGame(this.game.id)
      .subscribe((messages: MessageDto[]) => {
        messages.map(m => {
          this.chatSubject.getValue().push(new ChatMessage(m.name, m.content));
        });
      });
  }

  /**
   * sets up a websocket connection and watches (listens on) a certain topic where a client will receive chatmessages
   * new message will be pushed to an array inside an observable
   * this observable array [chatSubject] will be looped over asynchronously when using a pipe symbol and the async keyword
   */
  initializeChatConnection() {
    this.rxStompService.watch(`/chat/receive/${this.game.id}`)
      .subscribe((message) => {
        const parsedObj = JSON.parse(message.body);

        this.chatSubject.getValue().push(new ChatMessage(parsedObj.name, parsedObj.content));
      }, error => this.error = error.status);

    // setTimeout(() => this.sendMessage('scrabble', this.authService.getUserName() + ' joined the room.'), 500);
  }

  /**
   * sends a [[MessageDto]] to the websocket on a certain topic where other listeners can fetch the new chatmessage
   */
  sendMessage(name?: string, message?: string) {
    const messageDTO = new MessageDto(name, message, this.game.id);
    console.log(`(chat-component) sending this message to backend: ${messageDTO}`);
    this.rxStompService.publish({destination: `/chat/${this.game.id}/send`, body: JSON.stringify(messageDTO)});
    this.messageContentToSend = '';    // clears message after being sent
  }
}

