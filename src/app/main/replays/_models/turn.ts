import {Cell} from '../../board/_models/cell';
import {LetterRack} from '../../game/_models/letterrack';
import {Player} from '../../game/_models/player';

export class Turn {
  gameId: string;
  turnCounter: number;
  board: Cell[][];
  player: string;
  score: number;
  racks: LetterRack[];
  otherPlayers: Player[];

  constructor(turnCounter?: number, board?: Cell[][], player?: string, score?: number, racks?: LetterRack[], otherPlayers?: Player[]) {
    this.turnCounter = turnCounter;
    this.board = board;
    this.player = player;
    this.score = score;
    this.racks = racks;
    this.otherPlayers = otherPlayers;
  }
}
