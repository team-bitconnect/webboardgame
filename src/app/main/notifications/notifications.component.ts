import {Component, HostListener, OnInit, SimpleChanges} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Notification} from './_models/notification';
import {AuthenticationService} from '../../security/_services/authentication.service';
import {NotificationService} from '../_services/notification.service';
import {GameService} from '../_services/game.service';
import {Router} from '@angular/router';
import {RxStompService} from '@stomp/ng2-stompjs';
import {NotificationDTO} from './_dto/notification-dto';



/**
 * component to handle everything that has to do with notifications
 */
@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  notifsSubject = new BehaviorSubject<Notification[]>([]);

  constructor(private authService: AuthenticationService,
              private notifService: NotificationService,
              private gameService: GameService,
              private router: Router,
              private rxStompService: RxStompService) {
  }

  ngOnInit(): any {
    // load history of notifs
    this.notifService.getMyNotifications().subscribe((notifs: NotificationDTO[]) => {
      let notifz = notifs.map(n => new Notification(n.content, n.notifTitle, new Date(n.date), n.gameId, n.id));
      notifz = notifz.sort((a, b) => b.date.getTime() - a.date.getTime());
      this.notifsSubject.next(notifz);
    });

    this.initializeNotificationConnection();
  }

  private initializeNotificationConnection() {
    this.rxStompService.watch(`/notifications/receive/${this.authService.getUserName()}`)
      .subscribe((message) => {

        // map notifs
        const notifDTO: NotificationDTO = JSON.parse(message.body);
        console.log('--------------------\nparsed to NotificationDTO: ' + JSON.stringify(notifDTO));
        const notif = new Notification(notifDTO.content, notifDTO.notifTitle, new Date(notifDTO.date), notifDTO.gameId, notifDTO.id);

        // update notifs list
        this.notifsSubject.getValue().unshift(notif);
      }, error => console.log(error));
  }

  sendInvitationAnswer(n: Notification, answer: boolean) {
    if (answer) {
      this.gameService.updateInvitation(n.gameId, answer).subscribe(value => {
        console.log('invitation accepted result' + value);
      });
      this.deleteNotification(n);
    } else {
      this.deleteNotification(n);
    }
  }

  deleteNotification(n: Notification) {
    const index = this.notifsSubject.getValue().indexOf(n);
    this.notifsSubject.getValue().splice(index, 1);
    this.notifService.deleteNotification(n.id).subscribe(value => {
      console.log('invitation accepted result: ' + JSON.stringify(value));
    });
  }

  navigateToGame(n: Notification) {
    this.deleteNotification(n);
    this.router.navigate([`/game/${n.gameId}`]);
  }

  deleteAllNotifications() {
    this.notifsSubject.getValue().forEach((n: Notification) => {
      this.notifService.deleteNotification(n.id).subscribe(value => {
        console.log(`notif with id : ${n.id} deleted`);
      });
    });
    this.notifsSubject.next([]);
    console.log('all notifications deleted');
  }
}
