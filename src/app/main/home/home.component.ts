import {Component, HostListener, OnInit} from '@angular/core';
import {GameService} from '../_services/game.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  scrabbleLetters: Array<string> = [
    '../../assets/images/Solid_White/letter_S.png',
    '../../assets/images/Solid_White/letter_C.png',
    '../../assets/images/Solid_White/letter_R.png',
    '../../assets/images/Solid_White/letter_A.png',
    '../../assets/images/Solid_White/letter_B.png',
    '../../assets/images/Solid_White/letter_B.png',
    '../../assets/images/Solid_White/letter_L.png',
    '../../assets/images/Solid_White/letter_E.png',
  ];
  desktop: boolean;

  constructor() {
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.desktop = event.target.innerWidth >= 900;
  }

  ngOnInit(): any {
    this.desktop = window.innerWidth >= 900;
  }



}
