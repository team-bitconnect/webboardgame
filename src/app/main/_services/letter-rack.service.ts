import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

/**
 * service used to handle all the things related to filling and getting the letterracks of a logged in user
 */
@Injectable({providedIn: 'root'})
export class LetterRackService {

  constructor(private http: HttpClient) {
  }

  getLetterRackByGameId(id: string): Observable<any> {
    return this.http.get(`${environment.baseUrl}/api/letterracks/${id}`);
  }

  fillLetterRack(id: string, amountOfTiles: number) {
    return this.http.post(`${environment.baseUrl}/api/letterracks/${id}/tiles/random`,
      {amount: amountOfTiles});
  }
}
