import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {PlayerDTO} from '../game/_dto/player-dto';

/**
 * service used to handle all the things related to adding and fetching friends
 */
@Injectable({providedIn: 'root'})
export class FriendsService {

  constructor(private http: HttpClient) {
  }

  getAllFriends(): Observable<Array<PlayerDTO>> {
    return this.http.get<Array<PlayerDTO>>(`${environment.baseUrl}/api/friends`);
  }

  addFriend(newFriend: any): Observable<any> {
    console.log('adding new friend ' + newFriend);
    return this.http.post(`${environment.baseUrl}/api/friends`, new PlayerDTO(newFriend));
  }
}
