import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

/**
 * service used to handle all the things related to the chat
 */
@Injectable({providedIn: 'root'})
export class ChatService {

  constructor(private http: HttpClient) {
  }

  getMessagesForGame(id: string): Observable<any> {
    return this.http.get(`${environment.baseUrl}/api/chats/${id}/messages`);
  }
}
