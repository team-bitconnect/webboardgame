import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';

/**
 * service used to handle all the things related to boardcomponent
 */
@Injectable({
  providedIn: 'root'
})
export class BoardService {

  constructor() {
  }

  // this method calculates where the necessary images should be placed
  getImage(row: number, column: number): string {
    switch (environment.multipliers[row][column]) {
      case -3:
        return environment.tripleLetterScoreUrl;
      case -2:
        return environment.doubleLetterScoreUrl;
      case 1:
        return environment.starUrl;
      case 2:
        return environment.doubleWordScoreUrl;
      case 3:
        return environment.tripleWordScoreUrl;
    }
  }

  getCellId(row: number, column: number): string {
    if (row === 7 && column === 7) {
      return 'starLit';
    } else {
      return 'regularTile';
    }
  }

  getStyle(row: number, column: number): Object {
    return {
      'background-image': 'url(' + this.getImage(row, column) + ')',
      'background-size': '45px 45px'
    };
  }
}
