import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';


/**
 * service used to handle all the things related to statistics
 */
@Injectable({providedIn: 'root'})
export class StatisticService {

  constructor(private http: HttpClient) {
  }

  getStatistics(): Observable<any> {
    return this.http.get(`${environment.baseUrl}/api/statistics`);
  }
}
