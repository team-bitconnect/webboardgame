import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppComponent} from './app.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {LoginComponent} from './security/login/login.component';
import {AccountComponent} from './security/account/account.component';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClient, HttpHandler} from '@angular/common/http';

describe('AppComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, ReactiveFormsModule],
      declarations: [AppComponent, LoginComponent],
      providers : [HttpClient, HttpHandler],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  // it('should create the app', () => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   expect(app).toBeTruthy();
  // });

  //
  // it('when i click on the login button i should navigate to the home loginComponent', async(() => {
  //   // spies on submit method in logincomponent
  //   spyOn(loginComponent, 'onLogin');
  //
  //   // click on login button
  //   const loginButton = fixture.debugElement.nativeElement.querySelector('button');
  //   expect(loginButton).toBeTruthy();
  //   loginButton.click();
  //
  //   // check if submit button has been called and if we've landed on the correct url
  //   fixture.whenStable().then(() => {
  //     expect(loginComponent.onLogin).toHaveBeenCalled();
  //     expect(router.url).toBe('/');
  //   });
  // }));
});
